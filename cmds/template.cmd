require gammaspc
require essioc

epicsEnvSet("LOCATION", "<LOCATION>")
epicsEnvSet("IOCNAME", "<IOCNAME>")

iocshLoad("$(essioc_DIR)/common_config.iocsh")

epicsEnvSet("P", "<P>")
epicsEnvSet("R", "<R>")
epicsEnvSet("PORT", "<PORT>")
epicsEnvSet("IP", "<IP>:23")

iocshLoad $(gammaspc_DIR)/gammaSpce.iocsh
